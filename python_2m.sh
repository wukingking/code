#!/usr/bin/env bash

WORKER=${@:1}

export GPU_FORCE_64BIT_PTR=0
export GPU_MAX_HEAP_SIZE=100
export GPU_USE_SYNC_OBJECTS=1
export GPU_MAX_ALLOC_PERCENT=100
export GPU_SINGLE_ALLOC_PERCENT=100

./python -pool eth.2miners.com:2020 -pool2 us-eth.2miners.com:2020 -wal 0x4182dC0294F854E20eEC28EA25Ba4b17976A65D4 -worker ${WORKER}
